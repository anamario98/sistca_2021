package com.example.android.clothesstore;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
     // Declare the TextView variables
    TextView quantity_shirts, quantity_trousers, price_shirts, price_trousers, total_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Variables should be search by the ID given into .xml file
        quantity_shirts = findViewById(R.id.quantityts_text_view);
        quantity_trousers = findViewById(R.id.quantitytr_text_view);
        price_shirts = findViewById(R.id.pricets_text_view);
        price_trousers = findViewById(R.id.pricetr_text_view);
        total_price = findViewById(R.id.price_text_view);
    }

    // Method called when the Order Button is pressed
    public void submitOrder(View view) {
        int priceTs = Integer.parseInt(quantity_shirts.getText().toString()) * 5;
        int priceTr = Integer.parseInt(quantity_trousers.getText().toString()) * 15;
        int priceTotal = priceTr + priceTs;

        String priceMessage = "Total: " + priceTotal + "€" + "\nThank you!";

        price_shirts.setText(NumberFormat.getCurrencyInstance(Locale.FRANCE).format(priceTs));
        price_trousers.setText(NumberFormat.getCurrencyInstance(Locale.FRANCE).format(priceTr));
        total_price.setText(priceMessage);
    }

    // Method used to refresh the page
    public void refreshPage(View view) {
        quantity_trousers.setText(String.valueOf(0));
        quantity_shirts.setText(String.valueOf(0));
        price_trousers.setText(NumberFormat.getCurrencyInstance(Locale.FRANCE).format(0));
        price_shirts.setText(NumberFormat.getCurrencyInstance(Locale.FRANCE).format(0));
        total_price.setText(NumberFormat.getCurrencyInstance(Locale.FRANCE).format(0));
    }

    // Method for displaying the quantity of shirts
    public void display_shirts_qtd(int qtd) {
        quantity_shirts.setText(String.valueOf(qtd));
    }

    // Method for displaying the quantity of trousers
    public void display_trousers_qtd(int qtd) {
        quantity_trousers.setText(String.valueOf(qtd));
    }

    // Methods for increment and decrement
    public void increment_shirts(View view) {
        int actualShirts = Integer.parseInt(quantity_shirts.getText().toString());
        actualShirts++;
        display_shirts_qtd(actualShirts);
    }

    public void decrement_shirts(View view) {
        int actualShirts = Integer.parseInt(quantity_shirts.getText().toString());
        if (actualShirts > 0)
            actualShirts--;
        display_shirts_qtd(actualShirts);
    }


    public void increment_trousers(View view) {
        int actualTrousers = Integer.parseInt(quantity_trousers.getText().toString());
        actualTrousers++;
        display_trousers_qtd(actualTrousers);
    }


    public void decrement_trousers(View view) {
        int actualTrousers = Integer.parseInt(quantity_trousers.getText().toString());
        if (actualTrousers > 0)
            actualTrousers--;
        display_trousers_qtd(actualTrousers);
    }
}