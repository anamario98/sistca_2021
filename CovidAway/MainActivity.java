package com.example.android.covidaway;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Declaring variables
    CheckBox checkbox_fever, checkbox_fatigue, checkbox_cough,
            checkbox_sneezing, checkbox_nose, checkbox_throat,
            checkbox_diarrhea, checkbox_headache, checkbox_breath,
            checkbox_nauseous, checkbox_smell, checkbox_taste;
    TextView result_data;

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Variables search by the ID of the .xml code
        checkbox_fever = findViewById(R.id.checkbox_fever);
        checkbox_fatigue = findViewById(R.id.checkbox_fatigue);
        checkbox_cough = findViewById(R.id.checkbox_cough);
        checkbox_sneezing = findViewById(R.id.checkbox_sneezing);
        checkbox_nose = findViewById(R.id.checkbox_nose);
        checkbox_throat = findViewById(R.id.checkbox_throat);
        checkbox_diarrhea = findViewById(R.id.checkbox_diarrhea);
        checkbox_headache = findViewById(R.id.checkbox_headache);
        checkbox_breath = findViewById(R.id.checkbox_breath);
        checkbox_nauseous = findViewById(R.id.checkbox_nauseous);
        checkbox_smell = findViewById(R.id.checkbox_smell);
        checkbox_taste = findViewById(R.id.checkbox_taste);

        result_data = findViewById(R.id.result_data);

        image = findViewById(R.id.image);

    }

    // Method called when the Enter Button is pressed
    public void submitResult(View view) {
        int result = resultCovid();

        if(result==1){
            setCovidResult();
        }else if(result==0){
            setRareCovidResult();
        }else{
            setNoCovidResult();
        }

    }

    // Methods called to show the result
    public void setCovidResult(){
        result_data.setText("You have some habitual symptoms of covid-19");
        image.setImageResource(R.drawable.covid);
    }

    public void setRareCovidResult(){
        result_data.setText("You have some rare symptoms of covid-19, but you should pay attention to them");
        image.setImageResource(R.drawable.flu);
    }

    public void setNoCovidResult(){
        result_data.setText("You have no symptoms associated to covid-19");
        image.setImageResource(R.drawable.happy);
    }

    // Method to check the symptoms
    public int resultCovid() {
        if (checkbox_fever.isChecked() || checkbox_cough.isChecked() ||
                checkbox_headache.isChecked() || checkbox_breath.isChecked() ||
                checkbox_smell.isChecked() || checkbox_taste.isChecked()) {
            return 1;

        } else if (checkbox_fatigue.isChecked() || checkbox_nose.isChecked() ||
                checkbox_throat.isChecked() || checkbox_diarrhea.isChecked() ||
                checkbox_nauseous.isChecked()) {
            return 0;
        } else if (checkbox_sneezing.isChecked()) {
            return -1;
        }
        return -1;
    }
}

