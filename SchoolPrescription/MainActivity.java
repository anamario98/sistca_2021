package com.example.android.schoolprescription;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText name, entry_year, next_year, credits; // Define EditText variables
    TextView result_data;                         // Define TextView variables
    String profile = "";                          // Define the profile variable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Variables search by the ID of the .xml code
        name = findViewById(R.id.name);
        entry_year = findViewById(R.id.entry_year);
        next_year = findViewById(R.id.next_year);
        credits = findViewById(R.id.credits);
        result_data = findViewById(R.id.result_data);
    }

    // Method called when the Enter Button is pressed
    public void submitResult(View view) {

        String pName = name.getText().toString();
        // String variables to integer ones
        int startYear = Integer.parseInt(entry_year.getText().toString());
        int nextYear = Integer.parseInt(next_year.getText().toString());
        int creds = Integer.parseInt(credits.getText().toString());

        // Number of matriculations
        int matriculations = calculateNumberOfMatriculations(startYear, nextYear);

        // Prescribe or not
        boolean prescribe = calculatePerscription(creds, matriculations);

        if (prescribe) {
            result_data.setText("Hi," + profile + " " + pName + ", you're in risk of prescription!");
        } else {
            result_data.setText("Hi," + profile + " " + pName + ", it's everything ok!");
        }
    }

    // Method to calculate the number of matriculations
    public int calculateNumberOfMatriculations(int start, int next) {
        return next - start + 1;
    }

    // Method to set if the student is in danger of prescription or not
    public boolean calculatePerscription(int credits, int matriculations) {
        if (matriculations == 4 && credits < 60 || matriculations == 5 && credits < 120 ||
                matriculations == 6 && credits < 180 || matriculations == 8 && credits < 240 || matriculations == 9 && credits < 360)
            return true;
        else return false;
    }

    // Method for the RadioButtons- choose if is Mr. or Mrs.
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.female:
                if (checked)
                    profile = "Mrs.";
                break;
            case R.id.male:
                if (checked)
                    profile = "Mr.";
                break;
        }
    }


}


